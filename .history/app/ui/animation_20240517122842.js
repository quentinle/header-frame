let workLinks = document.querySelectorAll(".header-link");
console.log(workLinks);

workLinks.forEach((link, index, value) => {
  let underline = link.querySelector(".underline");
  console.log(link)
  link.tl = gsap.timeline({ paused: true });
  link.tl.fromTo(
    underline,
    {
      width: "0%",
      left: "0%",
    },
    {
      width: "100%",
      duration: 1,
    }
  );

  link.tl.add("midway");

  link.tl.fromTo(
    underline,
    {
      width: "100%",
      left: "0%",
    },
    {
      width: "0%",
      left: "100%",
      duration: 1,
      immediateRender: false,
    }
  );

  // Mouseenter
  link.addEventListener("mouseenter", (e) => {
    console.log('entering')
    enterAnimation(link, e, index);
  });

  // Mouseleave
  link.addEventListener("mouseleave", (e) => {
    leaveAnimation(link, e);
  });