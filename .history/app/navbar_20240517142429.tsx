export default function Navbar()
{
    return()
    {
        <div id="header" className="row align-items-center">
            <div className="col-sm-2">
              <Image
                src="/digital-booster.svg"
                alt="Digital booster Logo"
                className="dark:invert"
                width={185}
                height={82}
                priority
              />
            </div>
            <div className="col-sm-6 d-flex justify-content-evenly">
              <Link href="/" className="header-link">
                Bienvenue
                <span className="underline"></span>
              </Link>
              <Link href="/jobs" className="header-link">
                Nos métiers
                <span className="underline"></span>
              </Link>
              <Link href="/methodology" className="header-link">
                Notre méthodologie
                <span className="underline"></span>
              </Link>
              <Link href="/agency" className="header-link">
                L'agence
                <span className="underline"></span>
              </Link>
              <Link href="/blog" className="header-link">
                Blog
                <span className="underline"></span>
              </Link>
            </div>
            <div className="col-sm-4 d-flex align-items-center justify-content-end">
              <Link href="/contact" className="header-link" id="contact-link">
                Contact
                <span className="underline"></span>
              </Link>
              <button className="btn btn-primary">
                Prendre un rendez-vous
              </button>
            </div>
          </div>
    }
}