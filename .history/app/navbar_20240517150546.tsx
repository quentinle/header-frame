"use client";
import Image from "next/image";
import Link from "next/link";
import gsap from "gsap";
import { usePathname } from "next/navigation";

import { useEffect } from "react";
export default function Navbar() {
    const pathname = usePathname();
    useEffect(() => {
    //Header animation
    function enterAnimation(link: Element, e: Event, index: number) {
        link.tl.tweenFromTo(0, "midway");
    }
    function leaveAnimation(link: Element, e: Event) {
        link.tl.play();
    }
    // Animations variables
    let workLinkUnderlineAnimEnter;
    let workLinkUnderlineAnimLeave;

    //get all header link

    let workLinks = document.querySelectorAll(".header-link");
    workLinks.forEach((link, index, value) => {
        if(!link.classList.contains('active'))
            {

            }
        
    });
    });
    return (
        <div id="header" className="row align-items-center">
        <div className="col-sm-2">
          <Image
            src="/digital-booster.svg"
            alt="Digital booster Logo"
            className="dark:invert"
            width={185}
            height={82}
            priority
          />
        </div>
        <div className="col-sm-6 d-flex justify-content-evenly">
          <Link
            href="/"
            className={pathname == "/" ? "active header-link" : "header-link"}
          >
            Bienvenue
            <span className="underline"></span>
          </Link>
          <Link
            href="/jobs"
            className={pathname == "/jobs" ? "active header-link" : "header-link"}
          >
            Nos métiers
            <span className="underline"></span>
          </Link>
          <Link
            href="/methodology"
            className={pathname == "/methodology" ? "active header-link" : "header-link"}
          >
            Notre méthodologie
            <span className="underline"></span>
          </Link>
          <Link
            href="/agency"
            className={pathname == "/agency" ? "active header-link" : "header-link"}
          >
            L'agence
            <span className="underline"></span>
          </Link>
          <Link
            href="/blog"
            className={pathname == "/blog" ? "active header-link" : "header-link"}
          >
            Blog
            <span className="underline"></span>
          </Link>
        </div>
        <div className="col-sm-4 d-flex align-items-center justify-content-end">
          <Link
            href="/contact"
            className={pathname == "/contact" ? "active header-link" : "header-link"}
            id="contact-link"
          >
            Contact
            <span className="underline"></span>
          </Link>
          <button className="btn btn-primary">Prendre un rendez-vous</button>
        </div>
      </div>
    );
}
