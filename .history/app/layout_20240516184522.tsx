import type { Metadata } from "next";
import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du Sass
import Image from "next/image";
import Link from "next/link";

export const metadata: Metadata = {
  title: " Accueil | Digital Booster",
  description: "Page d'accueil",
};
//animation
function enterAnimation(link, e ,index){
  link.tl.tweenFromTo(0, "midway");
}
function leaveAnimation(link,e){
  link.tl.play();
}
// Animations variables
let workLinkUnderlineAnimEnter;
let workLinkUnderlineAnimLeave;

//get all header link
let workLinks = document.querySelectorAll(".header-link")

workLinks.forEach((link, index, value) => {
  let underline = link.querySelector(".underline");
  link.tl = gsap.timeline({})
})

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="fr">
      <body className={montserrat.className}>
        <main className="container">
          <div id="header" className="row align-items-center">
            <div className="col-sm-2">
              <Image
                src="/digital-booster.svg"
                alt="Digital booster Logo"
                className="dark:invert"
                width={185}
                height={82}
                priority
              />
            </div> 
            {/* Faire l'animation du hover text-decoration : underline */}
            <div className="col-sm-6 d-flex justify-content-evenly"> 
              <Link href="/" className="header-link">
                Bienvenue
              </Link>
              <Link href="/jobs" className="header-link">
                Nos métiers
              </Link>
              <Link href="/methodology" className="header-link">
                Notre méthodologie
              </Link>
              <Link href="/agency" className="header-link">
                L'agence
              </Link>
              <Link href="/blog" className="header-link">
                Blog
              </Link>
            </div>
            <div className="col-sm-4 d-flex align-items-center justify-content-end">
              <Link href="/contact" className="header-link" id="contact-link">
                Contact
              </Link>
              <button className="btn btn-primary">Prendre un rendez-vous</button>
            </div>
          </div>
          {children}
        </main>
      </body>
    </html>
  );
}
