"use client"
import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du Sass
import Image from "next/image";
import Link from "next/link";
import gsap from "gsap";
import { useEffect } from "react";
import Navbar from "./navbar";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>)
  {
    useEffect(() => {
      //Header animation
      function enterAnimation(link: Element, e: Event, index: number) {
        link.tl.tweenFromTo(0, "midway");
      }
      function leaveAnimation(link: Element, e: Event) {
        link.tl.play();
      }
      // Animations variables
      let workLinkUnderlineAnimEnter;
      let workLinkUnderlineAnimLeave;

      //get all header link
      
      let workLinks = document.querySelectorAll(".header-link");
      workLinks.forEach((link, index, value) => {
      let underline = link.querySelector(".underline");
      link.tl = gsap.timeline({ paused: true });
      link.tl.fromTo(
        underline,
        {
          width: "0%",
          left: "0%",
        },
        {
          width: "100%",
          duration: 0.3,
        }
      );

      link.tl.add("midway");

      link.tl.fromTo(
        underline,
        {
          width: "100%",
          left: "0%",
        },
        {
          width: "0%",
          left: "100%",
          duration: 0.5,
          immediateRender: false,
        }
      );
      // Mouseenter
      link.addEventListener("mouseenter", (e) => {
        enterAnimation(link, e, index);
      });

      // Mouseleave
      link.addEventListener("mouseleave", (e) => {
        leaveAnimation(link, e);
      });
    });
  })
  return (
    <html lang="fr">
      <body className={montserrat.className}>
        <main className="container">
          <Navbar></Navbar>
          {children}
        </main>
      </body>
    </html>
  );
}
