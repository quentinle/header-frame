import Image from "next/image";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Home() {
  return (
    <main className="container">
      <div className="row">
        <Image
          src="/digital-booster.svg"
          alt="Digital booster Logo"
          className="dark:invert"
          width={185}
          height={82}
          priority
        />
      </div>
      <h1 className="text-center">NextHS</h1>
    </main>
  );
}
