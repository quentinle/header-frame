/* eslint-disable react/no-unescaped-entities */
import Image from "next/image";
import Link from "next/link";

export default function Home() {
  return (
    <main className="container">
      <div id="header" className="row align-items-center">
        <div className="col-sm-2">
          <Image
            src="/digital-booster.svg"
            alt="Digital booster Logo"
            className="dark:invert"
            width={185}
            height={82}
            priority
          />
        </div>
        <div className="col-sm-6 d-flex justify-content-evenly">
          <Link href="/" className="header-link">Bienvenue</Link>
          <Link href="/" className="header-link">Nos métiers</div>
          <div className="header-link">Notre méthodologie</div>
          <div className="header-link">L'agence</div>
          <div className="header-link">Blog</div>
        </div>
        <div className="col-sm-4 d-flex align-items-center justify-content-end">
          <div className="header-link" id="contact-link">Contact</div>
          <button className="btn btn-primary">Prendre un rendez-vous</button>
        </div>
      </div>
      <h1 className="text-center">NextHS</h1>
    </main>
  );
}
