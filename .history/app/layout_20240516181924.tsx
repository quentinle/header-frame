import type { Metadata } from "next";
import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du 

export const metadata: Metadata = {
  title: " Accueil | Digital Booster",
  description: "Page d'accueil",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="fr">
      <body className={montserrat.className}>{children}</body>
    </html>
  );
}
