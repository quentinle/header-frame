import type { AppProps } from "next/app";
//obligatoire pour garder mes états d'animation 
export default function MyApp({ Component, pageProps }: AppProps) {
    return <Component {...pageProps} />;
}
