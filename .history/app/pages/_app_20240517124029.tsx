import type { AppProps } from "next/app";
import "../ui/animation.scss";
import "../ui/animation.js";
//obligatoire pour garder mes états d'animation dans la navbar
export default function MyApp({ Component, pageProps }: AppProps) {
    return <Component {...pageProps} />;
}
