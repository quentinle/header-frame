import type { AppProps } from "next/app";
import "../app/ui/animation.scss";
import "../app/ui/animation.js";
//obligatoire pour garder mes états d'animation dans la navbar
export default function MyApp({ Component, pageProps }: AppProps) {
    return <Component {...pageProps} />;
}
