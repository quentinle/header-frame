"use client"
import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du Sass

import Navbar from "./navbar";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>)
  {
  return (
    <html lang="fr">
      <body className={montserrat.className}>
        <main className="container">
          <Navbar></Navbar>
          {children}
        </main>
      </body>
    </html>
  );
}
