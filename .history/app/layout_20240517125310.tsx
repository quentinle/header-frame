import type { Metadata } from "next";
import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du Sass
import Image from "next/image";
import Link from "next/link";
import gsap from "gsap";
import Script from "next/script";

export const metadata: Metadata = {
  title: " Accueil | Digital Booster",
  description: "Page d'accueil",
};

export default function RootLayout({
  console.log('test');
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="fr">
      <body className={montserrat.className}>
        <Script src="/ui/animation.js" strategy="lazyOnload" />
        <main className="container">
          <div id="header" className="row align-items-center">
            <div className="col-sm-2">
              <Image
                src="/digital-booster.svg"
                alt="Digital booster Logo"
                className="dark:invert"
                width={185}
                height={82}
                priority
              />
            </div>
            {/* Faire l'animation du hover text-decoration : underline */}
            <div className="col-sm-6 d-flex justify-content-evenly">
              <Link href="/" className="header-link">
                Bienvenue
                <span className="underline"></span>
              </Link>
              <Link href="/jobs" className="header-link">
                Nos métiers
                <span className="underline"></span>
              </Link>
              <Link href="/methodology" className="header-link">
                Notre méthodologie
                <span className="underline"></span>
              </Link>
              <Link href="/agency" className="header-link">
                L'agence
                <span className="underline"></span>
              </Link>
              <Link href="/blog" className="header-link">
                Blog
                <span className="underline"></span>
              </Link>
            </div>
            <div className="col-sm-4 d-flex align-items-center justify-content-end">
              <Link href="/contact" className="header-link" id="contact-link">
                Contact
                <span className="underline"></span>
              </Link>
              <button className="btn btn-primary">
                Prendre un rendez-vous
              </button>
            </div>
          </div>
          {children}
        </main>
      </body>
    </html>
  );
}
