import Image from "next/image";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Home() {
  return (
    <main className="container">
      <div id="header" className="row align-items-center">
        <div className="col-sm-2">
          <Image
            src="/digital-booster.svg"
            alt="Digital booster Logo"
            className="dark:invert"
            width={185}
            height={82}
            priority
          />
          <div className="col-sm-6">
            <div className="header-link">Bienvenue</div>
            <div className="header-link">Nos métiers</div>
            <div className="header-link"></div>
            <div className="header-link"></div>
          </div>
          <div className="col-sm-4"></div>
        </div>
      </div>
      <h1 className="text-center">NextHS</h1>
    </main>
  );
}
