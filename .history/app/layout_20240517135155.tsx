"use client"
import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du Sass
import Image from "next/image";
import Link from "next/link";
import gsap from "gsap";
import { useEffect } from "react";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>)
  {
    useEffect(() => {
      //Header animation
      function enterAnimation(link: Element, e: Event, index: number) {
        link.tl.tweenFromTo(0, "midway");
      }
      function leaveAnimation(link: Element, e: Event) {
        link.tl.tweenFromTo("midway", "end");
      }
      // Animations variables
      let workLinkUnderlineAnimEnter;
      let workLinkUnderlineAnimLeave;

      //get all header link
      
      let workLinks = document.querySelectorAll(".header-link");
      workLinks.forEach((link, index, value) => {
      let underline = link.querySelector(".underline");
      link.tl = gsap.timeline({ paused: true });
      link.tl.fromTo(
        underline,
        {
          width: "0%",
          left: "0%",
        },
        {
          width: "100%",
          duration: 0.3,
        }
      );

      link.tl.add("midway");

      link.tl.fromTo(
        underline,
        {
          width: "100%",
          left: "0%",
        },
        {
          width: "0%",
          left: "100%",
          duration: 0.5,
          immediateRender: false,
        }
      );
      // Mouseenter
      link.addEventListener("mouseenter", (e) => {
        enterAnimation(link, e, index);
      });

      // Mouseleave
      link.addEventListener("mouseleave", (e) => {
        leaveAnimation(link, e);
      });
    });
  })
  return (
    <html lang="fr">
      <body className={montserrat.className}>
        <main className="container">
          <div id="header" className="row align-items-center">
            <div className="col-sm-2">
              <Image
                src="/digital-booster.svg"
                alt="Digital booster Logo"
                className="dark:invert"
                width={185}
                height={82}
                priority
              />
            </div>
            {/* Faire l'animation du hover text-decoration : underline */}
            <div className="col-sm-6 d-flex justify-content-evenly">
              <Link href="/" className="header-link">
                Bienvenue
                <span className="underline"></span>
              </Link>
              <Link href="/jobs" className="header-link">
                Nos métiers
                <span className="underline"></span>
              </Link>
              <Link href="/methodology" className="header-link">
                Notre méthodologie
                <span className="underline"></span>
              </Link>
              <Link href="/agency" className="header-link">
                L'agence
                <span className="underline"></span>
              </Link>
              <Link href="/blog" className="header-link">
                Blog
                <span className="underline"></span>
              </Link>
            </div>
            <div className="col-sm-4 d-flex align-items-center justify-content-end">
              <Link href="/contact" className="header-link" id="contact-link">
                Contact
                <span className="underline"></span>
              </Link>
              <button className="btn btn-primary">
                Prendre un rendez-vous
              </button>
            </div>
          </div>
          {children}
        </main>
      </body>
    </html>
  );
}
