"use client";
import Image from "next/image";
import Link from "next/link";
import gsap from "gsap";
import { usePathname } from "next/navigation";

import { useEffect } from "react";
export default function Navbar() {
  const pathname = usePathname();
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");

    //Header animation
    function enterAnimation(link: Element, e: Event, index: number) {
      link.tl?.tweenFromTo(0, "midway");
    }
    function leaveAnimation(link: Element, e: Event) {
      link.tl?.play();
    }

    //get all header link

    let workLinks = document.querySelectorAll(".header-link");
    workLinks.forEach((link, index, value) => {
      if (!link.classList.contains("active")) {
        let underline = link.querySelector(".underline");
        link.tl = gsap.timeline({ paused: true });
        link.tl.fromTo(
          underline,
          {
            width: "0%",
            left: "0%",
          },
          {
            width: "100%",
            duration: 0.3,
          }
        );

        link.tl.add("midway");

        link.tl.fromTo(
          underline,
          {
            width: "100%",
            left: "0%",
          },
          {
            width: "0%",
            left: "100%",
            duration: 0.5,
            immediateRender: false,
          }
        );
        // Mouseenter
        link.addEventListener("mouseenter", (e) => {
          enterAnimation(link, e, index);
        });

        // Mouseleave
        link.addEventListener("mouseleave", (e) => {
          leaveAnimation(link, e);
        });
      }
    });
  });
  return (
    <nav id="header" className="navbar navbar-expand-lg">
      <Link href="/">
        <Image
          src="/digital-booster.svg"
          alt="Digital booster Logo"
          className="dark:invert"
          width={185}
          height={82}
          priority
        />
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="navbar-collapse collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ms-5 me-auto">
          <li className="nav-item">
            <Link
              href="/"
              className={
                pathname == "/"
                  ? "active header-link nav-link"
                  : "header-link nav-link"
              }
            >
              Bienvenue
              <span className="underline"></span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              href="/jobs"
              className={
                pathname == "/jobs"
                  ? "active header-link"
                  : "header-link nav-link"
              }
            >
              Nos métiers
              <span className="underline"></span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              href="/methodology"
              className={
                pathname == "/methodology"
                  ? "active header-link"
                  : "header-link nav-link"
              }
            >
              Notre méthodologie
              <span className="underline"></span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              href="/agency"
              className={
                pathname == "/agency"
                  ? "active header-link"
                  : "header-link nav-link"
              }
            >
              {`L'agence`}
              <span className="underline"></span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              href="/blog"
              className={
                pathname == "/blog"
                  ? "active header-link"
                  : "header-link nav-link"
              }
            >
              Blog
              <span className="underline"></span>
            </Link>
          </li>
          <li className="nav-item"></li>
        </ul>
        <div className="d-flex align-items-center justify-content-end">
          <Link
            href="/contact"
            className={
              pathname == "/contact"
                ? "active header-link"
                : "header-link nav-link"
            }
            id="contact-link"
          >
            Contact
            <span className="underline"></span>
          </Link>
          <button className="btn btn-primary">Prendre un rendez-vous</button>
        </div>
      </div>
    </nav>
  );
}
