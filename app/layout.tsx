import { montserrat } from "./ui/fonts"; // import de la police principal
import "./globals.scss"; // import du Sass
import Navbar from "./navbar";


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>)
  {
  return (
    <html lang="fr">
      <link rel="icon" href="/digital-booster.svg"></link>
      <title>Digital Booster</title>
      <meta
        name="description"
        content="Digital booster accueil"
      />

      <body className={montserrat.className}>
        <main className="container">
          <Navbar></Navbar>
          {children}
        </main>
      </body>
    </html>
  );
}
